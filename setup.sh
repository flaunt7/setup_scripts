#!/bin/bash

HOSTNAME="server$1"

echo "Emptying Message of the Day"
echo "" > /etc/motd
echo "Done"
echo ""
echo "Setting hostname"
echo $HOSTNAME > /etc/hostname
echo "Done"

echo "Hostname change should take effect after next reboot"
echo "Reboot now? [y/N] "
read ans

if [ "$ans" == "Y" ] || [ "$ans" == "y" ]; then
    echo "Rebooting now"
    systemctl reboot -i
else
    echo "Okay. Have a nice day :)"
fi


